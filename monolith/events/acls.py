import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    url_params = {
        "query": f"{city},{state}",
        "per_page": "1",
    }
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    r = requests.get(url, params=url_params, headers=headers)
    c = json.loads(r.content)
    try:
        return {"picture_url": c["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    url_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": f"{city},{state},US",
    }
    r = requests.get(url, params=url_params)
    c = json.loads(r.content)
    lat = c[0]["lat"]
    lon = c[0]["lat"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=weather_params)
    content = json.loads(response.content)
    return {
        "temp(F)": content["main"]["temp"],
        "Weather Description": content["weather"][0]["description"],
    }
